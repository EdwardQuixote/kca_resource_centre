/**
 * 
 */
package com.edwardq.KCA.kcaresourcecentre.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardq.KCA.kcaresourcecentre.R;

/**
 * Created to host the Array Adapter for ListView on Home Activity.
 * 
 * Created on 30th January 2015
 * Time 1:17AM
 * 
 * @author Edward Quixote
 */
public class ArrayAdapterListHome extends ArrayAdapter<String> {
	
	private Context coxContext;
	
	private LayoutInflater linInflater;
	
	private String[] saryRowItems;

	public ArrayAdapterListHome(Context context, int resource, String[] objects) {
		super(context, resource, objects);
		
		this.coxContext = context;
		this.saryRowItems = objects;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder clsViewHolder = null;
		
		if (convertView == null) {
			
			clsViewHolder = new ViewHolder();
			
			linInflater = (LayoutInflater) coxContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			convertView = linInflater.inflate(R.layout.row_layout_list_home, parent, false);
			
			clsViewHolder.imgvIcon = (ImageView) convertView.findViewById(R.id.imgvRowLstHomeIcon);
			clsViewHolder.txtTitle = (TextView) convertView.findViewById(R.id.txtRowLstHomeTitle);
			clsViewHolder.txtInfo = (TextView) convertView.findViewById(R.id.txtRowLstHomeInfo);
			
			if (saryRowItems[0] == "Revision Notes") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once I acquire the Actual Icons
				clsViewHolder.txtTitle.setText(saryRowItems[0]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtLstRevisionNotes));
				
			} else if (saryRowItems[1] == "News") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once I acquire the actual icons
				clsViewHolder.txtTitle.setText(saryRowItems[1]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtLstNews));
				
			} else if (saryRowItems[2] == "Gallery") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once i get the Actual Icons
				clsViewHolder.txtTitle.setText(saryRowItems[2]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtGallery));
				
			} else {
				
				System.out.println("ListView data not being Loaded - try another way to do this.");	//	TODO: For Testing Only
				
				Toast.makeText(coxContext, "ListView Home not working as planned", Toast.LENGTH_SHORT).show();
				
			}
			
			convertView.setTag(clsViewHolder);
			
		} else {
			
			clsViewHolder = (ViewHolder) convertView.getTag();
			
			if (saryRowItems[0] == "Revision Notes") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once I acquire the Actual Icons
				clsViewHolder.txtTitle.setText(saryRowItems[0]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtLstRevisionNotes));
				
			} else if (saryRowItems[1] == "News") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once I acquire the actual icons
				clsViewHolder.txtTitle.setText(saryRowItems[1]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtLstNews));
				
			} else if (saryRowItems[2] == "Gallery") {
				
				clsViewHolder.imgvIcon.setImageResource(R.drawable.ic_launcher);	//	TODO: Change this once i get the Actual Icons
				clsViewHolder.txtTitle.setText(saryRowItems[2]);
				clsViewHolder.txtInfo.setText(coxContext.getResources().getString(R.string.txtGallery));
				
			} else {
				
				System.out.println("ListView data not being Loaded - try another way to do this.");	//	TODO: For Testing Only
				
				Toast.makeText(coxContext, "ListView Home not working as planned", Toast.LENGTH_SHORT).show();
				
			}
			
		}
		
		return convertView;
	}
	
	private class ViewHolder {
		
		private ImageView imgvIcon;
		
		private TextView txtTitle;
		private TextView txtInfo;
		
	}

}
