package com.edwardq.KCA.kcaresourcecentre;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.ListView;

import com.edwardq.KCA.kcaresourcecentre.CustomAdapters.ArrayAdapterListHome;

public class KCAResCenHomeActivity extends ActionBarActivity {
	
	private ArrayAdapterListHome clsAdpLstHome;
	
	private ListView lstHome;
	
	private String[] saryLstHomeItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kcares_cen_home);
        
        initializeVariablesAndUIObjects();
        
    }
    
    
    /**
     * In this method I initialize any objects and Variables used in UI.
     * 
     * Called in onCreate();
     */
    private void initializeVariablesAndUIObjects() {
    	
    	saryLstHomeItems = this.getResources().getStringArray(R.array.saryHomeList);
    	
    	clsAdpLstHome = new ArrayAdapterListHome(KCAResCenHomeActivity.this, R.layout.row_layout_list_home, saryLstHomeItems);
    	
    	lstHome = (ListView) findViewById(R.id.lstHome);
    	lstHome.setAdapter(clsAdpLstHome);
    	
    }

    
    @Override
    protected void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    }
    
    @Override
    protected void onRestart() {
    	super.onRestart();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.kcares_cen_home, menu);
        return true;
    }
    
}
